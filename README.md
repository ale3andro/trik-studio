# Το βιβλίο του Trik Studio

Το αποθετήριο αυτό περιέχει ένα βιβλίο για την εφαρμογή Trik Studio. Το βιβλίο αυτό γράφτηκε σταδιακά κατά τη διάρκεια προετοιμασίας μιας ομάδας μαθητριών και μαθητών για τη συμμετοχή τους στον Διαγωνισμό του WRO Hellas του 2020.

# Άδεια

[Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).